#include "MenuCommands.h"

#define LOCTEXT_NAMESPACE "FMenu_Item_PluginModule"

//Registers commands with a name, tooltip, action type, and hotkey gesture
void FMenuCommands::RegisterCommands()
{
	UI_COMMAND(MenuItem1, "Static Mesh Duplicator", "Displays Dockable Static Mesh Duplicator Window", EUserInterfaceActionType::Button, FInputGesture());
}

#undef LOCTEXT_NAMESPACE
