
#include "MyWindow.h"
#include "VitosLogMacros.h"

#include "Engine/World.h"
#include "Engine.h"
#include "LevelEditor.h"
#include "MultiBoxBuilder.h"
#include "GameFramework/Actor.h"
#include "Engine/StaticMeshActor.h"
#include "EngineGlobals.h"
#include "EngineUtils.h"
#include "Editor.h"
#include "Editor/EditorEngine.h"

//Widget Modules for UI elements
#include "SDockTab.h"
#include "STextBlock.h"
#include "SBox.h"
#include "SButton.h"
#include "SCheckBox.h"
#include "SNumericEntryBox.h"
#include "SComboButton.h"

//Reference to the tab
static const FName Menu_Item_PluginTabName("Menu_Item_Plugin");

#define LOCTEXT_NAMESPACE "Menu_Item_PluginModule"

// Constructor
MyWindow::MyWindow()
{
	/* Initialize values */

	NumCopies = 1;

	IsCheckedXPos = IsCheckedYPos = IsCheckedZPos = false;
	IsCheckedXNeg = IsCheckedYNeg = IsCheckedZNeg = false;

	SpacingAmountX = SpacingAmountY = SpacingAmountZ = 0.f;

	MyActor = nullptr;

	CopyCount = 0;

	FSlateFontInfo MyFont;
	MyFont.Size = 16;

	/* Checkboxes */

	// X

	CheckBoxXPos = SNew(SCheckBox)
		.Content()[SNew(STextBlock).Text(LOCTEXT("Checkbox_XPos", "+"))]
		.OnCheckStateChanged_Raw(this, &MyWindow::ToggleCheckBoxXPos)
		.IsChecked(ECheckBoxState::Unchecked);
	
	CheckBoxXNeg = SNew(SCheckBox)
		.Content()[SNew(STextBlock).Text(LOCTEXT("Checkbox_XNeg", "-"))]
		.OnCheckStateChanged_Raw(this, &MyWindow::ToggleCheckBoxXNeg)
		.IsChecked(ECheckBoxState::Unchecked);

	// Y

	CheckBoxYPos = SNew(SCheckBox)
		.Content()[SNew(STextBlock).Text(LOCTEXT("Checkbox_YPos", "+"))]
		.OnCheckStateChanged_Raw(this, &MyWindow::ToggleCheckBoxYPos)
		.IsChecked(ECheckBoxState::Unchecked);

	CheckBoxYNeg = SNew(SCheckBox)
		.Content()[SNew(STextBlock).Text(LOCTEXT("Checkbox_YNeg", "-"))]
		.OnCheckStateChanged_Raw(this, &MyWindow::ToggleCheckBoxYNeg)
		.IsChecked(ECheckBoxState::Unchecked);

	// Z

	CheckBoxZPos = SNew(SCheckBox)
		.Content()[SNew(STextBlock).Text(LOCTEXT("Checkbox_ZPos", "+"))]
		.OnCheckStateChanged_Raw(this, &MyWindow::ToggleCheckBoxZPos)
		.IsChecked(ECheckBoxState::Unchecked);

	CheckBoxZNeg = SNew(SCheckBox)
		.Content()[SNew(STextBlock).Text(LOCTEXT("Checkbox_ZNeg", "-"))]
		.OnCheckStateChanged_Raw(this, &MyWindow::ToggleCheckBoxZNeg)
		.IsChecked(ECheckBoxState::Unchecked);
}

// Deconstructor
MyWindow::~MyWindow()
{
}

//Registers the tab when UE4 opens
void MyWindow::RegisterTab()
{
	FGlobalTabmanager::Get()->RegisterNomadTabSpawner(Menu_Item_PluginTabName,
		FOnSpawnTab::CreateRaw(this, &MyWindow::OnSpawnPluginTab))
		// if TabMsg != null, use its title : else, use the backup title
		.SetDisplayName(LOCTEXT("TabTitle", "Static Mesh Duplicator"))
		.SetMenuType(ETabSpawnerMenuType::Hidden);
}

//Handles creating the tab when its menu item is clicked
void MyWindow::SpawnTab()
{
	FGlobalTabmanager::Get()->InvokeTab(Menu_Item_PluginTabName);
}

//Unregisters the tab when UE4 closes
void MyWindow::UnregisterTab()
{
	FGlobalTabmanager::Get()->UnregisterNomadTabSpawner(Menu_Item_PluginTabName);
}

// Duplicates the selected actor along one or more axes
FReply MyWindow::DuplicateButtonPressed()
{
	// Check if actor is not selected
	if (!MyActor)
	{
		LOGMSG(LogTemp, Error, "MyActor not found");

		FMessageDialog::Open(EAppMsgType::Ok, LOCTEXT("NoActorSelected", "Please select an actor from the dropdown menu."));

		return FReply::Handled();
	}

	// Check if no axis boxes are checked
	if (IsCheckedXPos == false && IsCheckedYPos == false && IsCheckedZPos == false
		&& IsCheckedXNeg == false && IsCheckedYNeg == false && IsCheckedZNeg == false)
	{
		LOGMSG(LogTemp, Error, "No axis selected");

		FMessageDialog::Open(EAppMsgType::Ok, LOCTEXT("NoAxisChecked", "Please select one or more axes."));

		return FReply::Handled();
	}

	// Begin creating copies
	for (int32 i = 0; i < NumCopies; i++)
	{
		// Gets the location and extends of the object to be copied
		MyActor->GetActorBounds(false, GroundOrigin, GroundExtends);

		// Increment CopyCount to be used in the new object's name
		CopyCount++;

		// Parse string from CopyCount
		StringCopyCount = FString::FromInt(CopyCount);

		// Get original actor's name
		MyActorName = MyActor->GetFName().ToString();

		// Build a new name out of the original name and copy count
		NewName = MyActorName + "_" + StringCopyCount;

		// Sets the name for the new copy
		SpawnParams.Name = FName(*NewName);

		//Sets the property values of the new actor to match the original actor
		SpawnParams.Template = MyActor;

		//Spawns the new actor on top of the original actor
		MyActorCopy = MyWorld()->SpawnActorAbsolute<AStaticMeshActor>(MyActor->GetClass(), MyActor->GetTransform(), SpawnParams);

		// If X axis is checked, calculate new X value, else, use the original X value
		if (IsCheckedXPos)
		{
			MyActorCopyX = ((i + 1) * GroundExtends.X * 2) + MyActorCopy->GetActorLocation().X + (SpacingAmountX * (i + 1));
		}
		else if (IsCheckedXNeg)
		{
			MyActorCopyX = -((i + 1) * GroundExtends.X * 2) + MyActorCopy->GetActorLocation().X + (-SpacingAmountX * (i + 1));
		}
		else
		{
			MyActorCopyX = MyActor->GetActorLocation().X;
		}

		// If Y axis is checked, calculate new Y value, else, use the original Y value
		if (IsCheckedYPos)
		{
			MyActorCopyY = ((i + 1) * GroundExtends.Y * 2) + MyActorCopy->GetActorLocation().Y + (SpacingAmountY * (i + 1));
		}
		else if (IsCheckedYNeg)
		{
			MyActorCopyY = -((i + 1) * GroundExtends.Y * 2) + MyActorCopy->GetActorLocation().Y + (-SpacingAmountY * (i + 1));
		}
		else
		{
			MyActorCopyY = MyActor->GetActorLocation().Y;
		}

		// If Z axis is checked, calculate new Z value, else, use the original Z value
		if (IsCheckedZPos)
		{
			MyActorCopyZ = ((i + 1) * GroundExtends.Z * 2) + MyActorCopy->GetActorLocation().Z + (SpacingAmountZ * (i + 1));
		}
		else if (IsCheckedZNeg)
		{
			MyActorCopyZ = -((i + 1) * GroundExtends.Z * 2) + MyActorCopy->GetActorLocation().Z + (-SpacingAmountZ * (i + 1));
		}
		else
		{
			MyActorCopyZ = MyActor->GetActorLocation().Z;
		}

		//Moves the new copy to the axis positions that were just calculated above
		MyActorCopy->SetActorLocation(FVector(MyActorCopyX, MyActorCopyY, MyActorCopyZ));

		//LOGMSG_M(LogTemp, Warning, "Spawned Copy #%d - [%s]", CopyCount, *MyActorCopy->GetName());

	} //endfor

	//LOGMSG_M(LogTemp, Warning, "Finished Spawning Copies of [%s]", *MyActor->GetName());

	return FReply::Handled();
}

//Toggles the state of the X axis checkbox
void MyWindow::ToggleCheckBoxXPos(ECheckBoxState NewState)
{
	// If NewState == Checked, set IsCheckedXPos to true : else, set to false
	IsCheckedXPos = NewState == ECheckBoxState::Checked ? true : false;

	if (NewState == ECheckBoxState::Checked)
	{
		//LOGMSG(LogTemp, Warning, "Box Checked!");

		// If the other direction is checked, uncheck it
		if (IsCheckedXNeg) CheckBoxXNeg->ToggleCheckedState();
	}
	else if (NewState == ECheckBoxState::Unchecked)
	{
		//LOGMSG(LogTemp, Warning, "Box Unchecked!");
	}
	else
	{
		//LOGMSG(LogTemp, Warning, "CheckBox State Undetermined");
	}
}

void MyWindow::ToggleCheckBoxXNeg(ECheckBoxState NewState)
{
	// If NewState == Checked, set IsCheckedXNeg to true : else, set to false
	IsCheckedXNeg = NewState == ECheckBoxState::Checked ? true : false;

	if (NewState == ECheckBoxState::Checked)
	{
		//LOGMSG(LogTemp, Warning, "Box Checked!");

		// If the other direction is checked, uncheck it
		if (IsCheckedXPos) CheckBoxXPos->ToggleCheckedState();
	}
	else if (NewState == ECheckBoxState::Unchecked)
	{
		//LOGMSG(LogTemp, Warning, "Box Unchecked!");
	}
	else
	{
		//LOGMSG(LogTemp, Warning, "CheckBox State Undetermined");
	}
}

//Toggles the state of the Y axis checkbox
void MyWindow::ToggleCheckBoxYPos(ECheckBoxState NewState)
{
	// If NewState == Checked, set IsCheckedYPos to true : else, set to false
	IsCheckedYPos = NewState == ECheckBoxState::Checked ? true : false;

	if (NewState == ECheckBoxState::Checked)
	{
		//LOGMSG(LogTemp, Warning, "Box Checked!");

		// If the other direction is checked, uncheck it
		if (IsCheckedYNeg) CheckBoxYNeg->ToggleCheckedState();
	}
	else if (NewState == ECheckBoxState::Unchecked)
	{
		//LOGMSG(LogTemp, Warning, "Box Unchecked!");
	}
	else
	{
		//LOGMSG(LogTemp, Warning, "CheckBox State Undetermined");
	}
}

void MyWindow::ToggleCheckBoxYNeg(ECheckBoxState NewState)
{
	// If NewState == Checked, set IsCheckedYNeg to true : else, set to false
	IsCheckedYNeg = NewState == ECheckBoxState::Checked ? true : false;

	if (NewState == ECheckBoxState::Checked)
	{
		//LOGMSG(LogTemp, Warning, "Box Checked!");

		// If the other direction is checked, uncheck it
		if (IsCheckedYPos) CheckBoxYPos->ToggleCheckedState();
	}
	else if (NewState == ECheckBoxState::Unchecked)
	{
		//LOGMSG(LogTemp, Warning, "Box Unchecked!");
	}
	else
	{
		//LOGMSG(LogTemp, Warning, "CheckBox State Undetermined");
	}
}

//Toggles the state of the Z axis checkbox
void MyWindow::ToggleCheckBoxZPos(ECheckBoxState NewState)
{
	// If NewState == Checked, set IsCheckedZPos to true : else, set to false
	IsCheckedZPos = NewState == ECheckBoxState::Checked ? true : false;

	if (NewState == ECheckBoxState::Checked)
	{
		//LOGMSG(LogTemp, Warning, "Box Checked!");

		// If the other direction is checked, uncheck it
		if (IsCheckedZNeg) CheckBoxZNeg->ToggleCheckedState();
	}
	else if (NewState == ECheckBoxState::Unchecked)
	{
		//LOGMSG(LogTemp, Warning, "Box Unchecked!");
	}
	else
	{
		//LOGMSG(LogTemp, Warning, "CheckBox State Undetermined");
	}
}

void MyWindow::ToggleCheckBoxZNeg(ECheckBoxState NewState)
{
	// If NewState == Checked, set IsCheckedZNeg to true : else, set to false
	IsCheckedZNeg = NewState == ECheckBoxState::Checked ? true : false;

	if (NewState == ECheckBoxState::Checked)
	{
		//LOGMSG(LogTemp, Warning, "Box Checked!");

		// If the other direction is checked, uncheck it
		if (IsCheckedZPos) CheckBoxZPos->ToggleCheckedState();
	}
	else if (NewState == ECheckBoxState::Unchecked)
	{
		//LOGMSG(LogTemp, Warning, "Box Unchecked!");
	}
	else
	{
		//LOGMSG(LogTemp, Warning, "CheckBox State Undetermined");
	}
}

// Gets the number of copies
TOptional<int32> MyWindow::GetNumCopies() const
{
	return NumCopies;
}

// Sets the number of copies
void MyWindow::SetNumCopies(int32 NewValue)
{
	if (NewValue < 1)
	{
		//LOGMSG(LogTemp, Error, "The minimum number of copies is 1 - Number of Copies has been set to 1");

		NumCopies = 1;
	}
	else
	{
		NumCopies = NewValue;
	}
}

// Gets spacing amount on the X axis
TOptional<float> MyWindow::GetSpacingAmountX() const
{
	return SpacingAmountX;
}

// Gets spacing amount on the Y axis
TOptional<float> MyWindow::GetSpacingAmountY() const
{
	return SpacingAmountY;
}

// Gets spacing amount on the Z axis
TOptional<float> MyWindow::GetSpacingAmountZ() const
{
	return SpacingAmountZ;
}

// Sets spacing amount on the X axis
void MyWindow::SetSpacingAmountX(float NewValue)
{
	if (NewValue < 0.f)
	{
		LOGMSG(LogTemp, Error, "Negative values are not supported - Spacing Amount on the X axis has been set to 0");

		SpacingAmountX = 0.f;
	}
	else
	{
		SpacingAmountX = NewValue;
	}
}

// Sets spacing amount on the Y axis
void MyWindow::SetSpacingAmountY(float NewValue)
{
	if (NewValue < 0.f)
	{
		LOGMSG(LogTemp, Error, "Negative values are not supported - Spacing Amount on the Y axis has been set to 0");

		SpacingAmountY = 0.f;
	}
	else
	{
		SpacingAmountY = NewValue;
	}
}

// Sets spacing amount on the Z axis
void MyWindow::SetSpacingAmountZ(float NewValue)
{
	if (NewValue < 0.f)
	{
		LOGMSG(LogTemp, Error, "Negative values are not supported - Spacing Amount on the Z axis has been set to 0");

		SpacingAmountZ = 0.f;
	}
	else
	{
		SpacingAmountZ = NewValue;
	}
}

// Gets the contexts of the world
UWorld * MyWindow::MyWorld()
{
	if (!GEngine)
	{
		LOGMSG(LogTemp, Error, "GEngine not found");

		return nullptr;
	}

	UWorld* World = nullptr;

	const TIndirectArray<FWorldContext>& MyContexts = GEngine->GetWorldContexts();

	for (int32 i = 0; i < MyContexts.Num(); i++)
	{
		if (MyContexts[i].WorldType == EWorldType::PIE)
		{
			//LOGMSG(LogTemp, Log, "Playing In Editor");

			return MyContexts[i].World();
		}

		if (MyContexts[i].WorldType == EWorldType::Editor)
		{
			//LOGMSG(LogTemp, Log, "Editing In Editor");

			World = MyContexts[i].World();
		}
	} //end for

	return World;
}

// Returns the actor's name or "<No Actor Selected>"
FText MyWindow::GetActorName() const
{
	FText ActorName = (MyActor) ?
		FText::FromString(MyActor->GetName()) :
		LOCTEXT("NoActorSelected", "<No Actor Selected>");

	return FText::Format(LOCTEXT("ComboBoxLabel", "Actor: {0}"), ActorName);
}

// Returns true if MyActor is the passed-in SelectedActor
bool MyWindow::IsActorSelected(AStaticMeshActor* SelectedActor)
{
	return SelectedActor == MyActor;
}

// Sets MyActor to the SelectedActor
void MyWindow::ActorSelected(AStaticMeshActor* SelectedActor)
{
	MyActor = SelectedActor;

	// Selects the actor in the editor
	if (GEditor)
	{
		GEditor->SelectNone(true, false);
		GEditor->SelectActor(SelectedActor, true, true);
	}
}

// Reverts MyActor back to nullptr
void MyWindow::DeselectActors()
{
	MyActor = nullptr;

	// Deselects all actors in the editor
	if (GEditor) GEditor->SelectNone(true, false);
}

// Lists static mesh actors available to be selected
TSharedRef<SWidget> MyWindow::DisplayActorList()
{
	UWorld* World = MyWorld();
	FMenuBuilder menu(true, nullptr);

	// Adds a menu entry to select none (deselect actors)
	menu.AddMenuEntry(
		FText::FromString("None"),
		FText::FromString("Deselect Actors"),
		FSlateIcon(),
		FUIAction(
			FExecuteAction::CreateRaw(this, &MyWindow::DeselectActors),
			FCanExecuteAction()),
		NAME_None,
		EUserInterfaceActionType::Button
	);

	// Iterates through static mesh actors in the world and adds them to the list
	for (TActorIterator<AStaticMeshActor> It(World); It; ++It)
	{
		menu.AddMenuEntry(
			FText::FromString(It->GetName()),
			FText::FromString(It->GetName()),
			FSlateIcon(),
			FUIAction(
				FExecuteAction::CreateRaw(this, &MyWindow::ActorSelected, *It),
				FCanExecuteAction(),
				FIsActionChecked::CreateRaw(this, &MyWindow::IsActorSelected, *It)
			),
			NAME_None,
			EUserInterfaceActionType::ToggleButton
		);
	}

	return menu.MakeWidget();
}

//Populates the tab with widgets when it is created/spawned
TSharedRef<class SDockTab> MyWindow::OnSpawnPluginTab(const FSpawnTabArgs& SpawnTabArgs)
{
	FText DefaultNoActor = LOCTEXT("NoActorSelected", "< No Actor Selected >");
	
	return SNew(SDockTab)
		.TabRole(ETabRole::NomadTab) //NomadTab = Dockable Tab
		[
			/*******************************/
			/****** BEGIN TAB CONTENT ******/

			SNew(SVerticalBox)
			// Row 1 ---------------------------------------------------------------------- Row 1
			+ SVerticalBox::Slot().AutoHeight()
			[
				// Text
				SNew(STextBlock).Text(LOCTEXT("ActorDuplicatorTitle", "Vito's Actor Duplicator"))
					.ColorAndOpacity(YELLOW)
					.ShadowColorAndOpacity(BLACK)
			]

			// Row 2 ---------------------------------------------------------------------- Row 2
			+ SVerticalBox::Slot().AutoHeight().Padding(10)
			[
				// ComboButon
				SAssignNew(ActorDropdown, SComboButton)
					.ButtonContent()[SNew(STextBlock).Text_Raw(this, &MyWindow::GetActorName)]
					.OnGetMenuContent_Raw(this, &MyWindow::DisplayActorList)
			]

			// Row 3 ---------------------------------------------------------------------- Row 3
			+ SVerticalBox::Slot().AutoHeight().Padding(5,15)
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot().AutoWidth().Padding(0,0,10,0) // Column 1
				[
					// Text
					SNew(STextBlock).Text(LOCTEXT("CopiesLabel", "Number of Copies"))
				]
				+ SHorizontalBox::Slot() // Column 2
				[
					// NumEntBox
					SNew(SNumericEntryBox<int32>) // NumCopiesX
						.Value_Raw(this, &MyWindow::GetNumCopies)
						.OnValueChanged_Raw(this, &MyWindow::SetNumCopies)
				]
			]

			// Row 4 ---------------------------------------------------------------------- Row 4
			+ SVerticalBox::Slot().AutoHeight().Padding(5)
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot() // Column 1
				[
					// Text
					SNew(STextBlock).Text(LOCTEXT("AxisLabel", "Axis"))
						.ColorAndOpacity(CYAN)
				]

				+ SHorizontalBox::Slot() // Column 2
				[
					// Text
					SNew(STextBlock).Text(LOCTEXT("DirectionLabel", "Direction"))
						.ColorAndOpacity(CYAN)
				]

				+ SHorizontalBox::Slot() // Column 3
				[
					// Text
					SNew(STextBlock).Text(LOCTEXT("SpacingLabel", "Spacing"))
						.ColorAndOpacity(CYAN)
						.Justification(ETextJustify::Right)
				]
			]

			// Row 5 ---------------------------------------------------------------------- Row 5
			+ SVerticalBox::Slot().AutoHeight().Padding(5)
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot().VAlign(VAlign_Center) // Column 1
				[
					// Text
					SNew(STextBlock).Text(LOCTEXT("AxisLabelX", "X")).Margin(FMargin(10,0,0,0))
				]
				
				// Column 2
				+ SHorizontalBox::Slot().HAlign(HAlign_Center) 
				[ CheckBoxXNeg.ToSharedRef() ] // CheckBox

				// Column 3
				+ SHorizontalBox::Slot() 
				[ CheckBoxXPos.ToSharedRef() ] // CheckBox

				// Column 4
				+ SHorizontalBox::Slot().Padding(5) 
				[
					// NumEntBox
					SNew(SNumericEntryBox<float>) // SpacingAmountX
						.Value_Raw(this, &MyWindow::GetSpacingAmountX)
						.OnValueChanged_Raw(this, &MyWindow::SetSpacingAmountX)
				]
			]

			// Row 6 ---------------------------------------------------------------------- Row 6
			+ SVerticalBox::Slot().AutoHeight().Padding(5)
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot().VAlign(VAlign_Center) // Column 1
				[
					// Text
					SNew(STextBlock).Text(LOCTEXT("AxisLabelY", "Y")).Margin(FMargin(10, 0, 0, 0))
				]

				// Column 2
				+ SHorizontalBox::Slot().HAlign(HAlign_Center)
				[CheckBoxYNeg.ToSharedRef()] // CheckBox

				// Column 3
				+ SHorizontalBox::Slot()
				[CheckBoxYPos.ToSharedRef()] // CheckBox

				// Column 4
				+ SHorizontalBox::Slot().Padding(5)
				[
					// NumEntBox
					SNew(SNumericEntryBox<float>) // SpacingAmountY
						.Value_Raw(this, &MyWindow::GetSpacingAmountY)
						.OnValueChanged_Raw(this, &MyWindow::SetSpacingAmountY)
				]
			]

			// Row 7 ---------------------------------------------------------------------- Row 7
			+ SVerticalBox::Slot().AutoHeight().Padding(5)
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot().VAlign(VAlign_Center) // Column 1
				[
					// Text
					SNew(STextBlock).Text(LOCTEXT("AxisLabelZ", "Z")).Margin(FMargin(10, 0, 0, 0))
				]

				// Column 2
				+ SHorizontalBox::Slot().HAlign(HAlign_Center)
				[CheckBoxZNeg.ToSharedRef()] // CheckBox

				// Column 3
				+ SHorizontalBox::Slot()
				[CheckBoxZPos.ToSharedRef()] // CheckBox

				// Column 4
				+ SHorizontalBox::Slot().Padding(5)
				[
					// NumEntBox
					SNew(SNumericEntryBox<float>) // SpacingAmountZ
						.Value_Raw(this, &MyWindow::GetSpacingAmountZ)
						.OnValueChanged_Raw(this, &MyWindow::SetSpacingAmountZ)
				]
			]

			// Row 8 ---------------------------------------------------------------------- Row 8
			+ SVerticalBox::Slot().AutoHeight().Padding(10)
			[
				// Button
				SNew(SButton).Text(LOCTEXT("DuplicateButtonLabel", "Click to Duplicate!")).HAlign(HAlign_Center).VAlign(VAlign_Center)
					.OnClicked_Raw(this, &MyWindow::DuplicateButtonPressed)
			]

			/******* END TAB CONTENT *******/
			/*******************************/
		];
}
