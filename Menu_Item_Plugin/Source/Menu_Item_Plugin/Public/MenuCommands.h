#pragma once

#include "SlateBasics.h"
#include "MenuStyle.h"

class FMenuCommands : public TCommands<FMenuCommands>
{
public:

	FMenuCommands()
		: TCommands<FMenuCommands>(TEXT("Menu_Item_Plugin"),
			NSLOCTEXT("Contexts", "Menu", "Menu Plugin"),
			NAME_None,
			FMenuStyle::GetStyleSetName())
	{
	}

	// TCommands<> interface
	virtual void RegisterCommands() override;

public:
	TSharedPtr< FUICommandInfo > MenuItem1;
};
